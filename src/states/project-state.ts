import {Project , ProjectStatus} from '../models/project'

    type Listeners<T> = (items: T[]) => void;

    class State<T>{
        protected listeners: Listeners<T>[] = [];

        addListeners(listenersFn: Listeners<T>) {
            this.listeners.push(listenersFn);

        }
    }

    // Project state management class

    export class ProjectState extends State<Project> {
        private projects: Project[] = [];
        private static instance: ProjectState;
        private constructor() {
            super()
        }

        static getInstance() {
            if (this.instance) {
                return this.instance
            }

            this.instance = new ProjectState();
            return this.instance
        }



        addProject(title: string, description: string, numOfPeople: number) {
            const newProject = new Project(Math.random().toString(), title, description, numOfPeople, ProjectStatus.Active)


            this.projects.push(newProject);
            console.log(this.projects, "projects")
            this.updateListeners();
        }

        moveProject(projectId: string, newStatus: ProjectStatus) {
            const project = this.projects.find(prj => prj.id === projectId)
            if (project && project.status !== newStatus) {
                project.status = newStatus;
                this.updateListeners();
            }
        }

        private updateListeners() {
            for (const listenersFn of this.listeners) {
                // console.log(listenersFn)
                listenersFn(this.projects.slice());
            }
        }
    }
    export const projectState = ProjectState.getInstance();

