import {Component} from './base-components';
import * as Validation from '../util/validation';
import {autobind as Autobind} from '../decorators/autobind';
import {projectState} from '../states/project-state'



  export  class ProjectInput extends Component<HTMLDivElement, HTMLFormElement> {
        titleInputElement: HTMLInputElement;
        descriptionInputElement: HTMLInputElement;
        peopleInputElement: HTMLInputElement;

        constructor() {
            super('project-input', 'app', true, 'user-input')
            this.titleInputElement = this.element.querySelector('#title')! as HTMLInputElement;
            this.descriptionInputElement = this.element.querySelector('#description')! as HTMLInputElement;
            this.peopleInputElement = this.element.querySelector('#people')! as HTMLInputElement;
            this.configure();
            this.renderContent();

        }

        private clearInput() {
            this.titleInputElement.value = '';
            this.descriptionInputElement.value = '';
            this.peopleInputElement.value = '';

        }

        private gatherUserInput(): [string, string, number] | void {
            const enteredTitle = this.titleInputElement.value;
            const enteredDescriptor = this.descriptionInputElement.value;
            const enteredPeople = this.peopleInputElement.value;

            const titleValidate: Validation.validation = {
                value: enteredTitle,
                required: true
            }

            const descriptionValidate: Validation.validation = {
                value: enteredDescriptor,
                required: true,
                minLength: 6
            }

            const peopleValidate: Validation.validation = {
                value: +enteredPeople,
                required: true,
                min: 1,
                max: 6
            }

            if (!Validation.validate(titleValidate) ||
                !Validation.validate(descriptionValidate) ||
                !Validation.validate(peopleValidate)
            ) {
                alert('invalid user Input');
                return;
            }
            else {
                return [enteredTitle, enteredDescriptor, +enteredPeople];
            }
        }

        @Autobind
        private submitHandler(event: Event) {

            event.preventDefault()
            const userInput = this.gatherUserInput();
            if (Array.isArray(userInput)) {
                const [title, desc, people] = userInput;
                projectState.addProject(title, desc, people);
                this.clearInput();

            }
        }

        configure() {
            this.element.addEventListener('submit', this.submitHandler)
        }

        renderContent() { }


    }
